package com.wrighton.rpn;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;


public class MainActivity extends Activity {

    protected TextView expressionText;
    protected TextView resultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        expressionText = (TextView)findViewById(R.id.expressionText);
        resultText = (TextView)findViewById(R.id.resultText);
        expressionText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    evaluate();
                    return true;
                }
                return false;
            }
        });
    }

    public void evaluate() {
        String expression = expressionText.getText().toString();
        RPNEvaluator evaluator = new RPNEvaluator();
        String result = "";
        try {
            result = "Result: " + evaluator.evaluate(expression);
        } catch (Exception e) {
            result = e.getMessage();
        }
        resultText.setText(result);
    }
}
