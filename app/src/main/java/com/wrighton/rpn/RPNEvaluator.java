package com.wrighton.rpn;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Class to evaluate a Reverse Polish Notation expression.
 */
public class RPNEvaluator {

    enum TokenType {
        NUMBER,
        OP_ADD,
        OP_SUBTRACT,
        OP_MULTIPLY,
        OP_DIVIDE,
        INVALID
    }

    /**
     * Token represents an operator or numerical value.
     * TODO: refactor this so that operators don't have a
     * value associated with them. Also, reusing tokens to
     * store intermediate values can probably be improved.
     */
    class Token {
        public TokenType type;
        public double value;
        public Token(TokenType type) {
            this.type = type;
        }
    }

    /**
     * Convert the expression string into a list of Tokens.
     * Currently, invalid tokens are marked as such and ignored. We
     * may wish to just throw an Exception instead.

     * @param input expression string
     * @return the Token queue.
     */
    protected Queue<Token> inputToTokens(String input) {
        Queue<Token> tokens = new LinkedList();
        int i = 0;

        while (i < input.length()) {
            char c = input.charAt(i);
            if (Character.isDigit(c)) {
                Token token = new Token(TokenType.NUMBER);
                StringBuffer value = new StringBuffer();
                while (i < input.length() && Character.isDigit(input.charAt(i))) {
                    value.append(input.charAt(i));
                    i++;
                }
                token.value = Double.parseDouble(value.toString());
                tokens.add(token);
                continue;
            }
            if (c == '+') {
                tokens.add(new Token(TokenType.OP_ADD));
            } else if (c == '-') {
                tokens.add(new Token(TokenType.OP_SUBTRACT));
            } else if (c == '*') {
                tokens.add(new Token(TokenType.OP_MULTIPLY));
            } else if (c == '/') {
                tokens.add(new Token(TokenType.OP_DIVIDE));
            } else if (c != ' ') {
                tokens.add(new Token(TokenType.INVALID));
            }
            i++;
        }

        return tokens;
    }

    /**
     * Evaluate the expression using an expression-stack algorithm.
     * @param expression
     * @return the numerical result as a string
     * @throws Exception when the expression contains the wrong number of operands
     */
    public String evaluate(String expression) throws Exception {
        Queue<Token> tokens = inputToTokens(expression);
        Stack<Token> stack = new Stack();

        while (!tokens.isEmpty()) {
            Token token = tokens.remove();
            if (token.type == TokenType.NUMBER) {
                stack.push(token);
            } else if (token.type == TokenType.OP_ADD
                    || token.type == TokenType.OP_SUBTRACT
                    || token.type == TokenType.OP_MULTIPLY
                    || token.type == TokenType.OP_DIVIDE) {
                if (stack.size() < 2) {
                    throw new Exception("Not enough operands for evaluation");
                }
                Token op2 = stack.pop();
                Token op1 = stack.pop();
                if (op1.type != TokenType.NUMBER
                        || op2.type != TokenType.NUMBER) {
                    throw new Exception("Invalid operands");
                }
                Token result = compute(op1, op2, token);
                stack.push(result);
            }
        }

        if (stack.size() != 1) {
            throw new Exception("To many operands in expression");
        }

        return stack.pop().value + "";
    }

    protected Token compute(Token op1, Token op2, Token operator) {
        Token result = new Token(TokenType.NUMBER);

        if (operator.type == TokenType.OP_ADD) {
            result.value = op1.value + op2.value;
        } else if (operator.type == TokenType.OP_SUBTRACT) {
            result.value = op1.value - op2.value;
        } else if (operator.type == TokenType.OP_MULTIPLY) {
            result.value = op1.value * op2.value;
        } else if (operator.type == TokenType.OP_DIVIDE) {
            result.value = op1.value / op2.value;
        }

        return result;
    }
}
